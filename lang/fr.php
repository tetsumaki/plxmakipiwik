<?php

$LANG = array(

'L_PROTOCOL' 	=> 'Protocole',
'L_URL' 		=> 'Adresse sans le / à la fin',
'L_ID'			=> 'ID du site',
'L_NAME_JS'		=> 'Nom du tracker JS',
'L_NAME_PHP'	=> 'Nom du tracker PHP',
'L_JS'			=> 'Tracking JavaScript',
'L_IMG'			=> 'Tracking Image',
'L_SAVE'		=> 'Enregistrer',

);
?>