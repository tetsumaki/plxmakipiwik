<?php

$LANG = array(

'L_PROTOCOL' 	=> 'Protocol',
'L_URL' 		=> 'Address without / at the end',
'L_ID'			=> 'Site ID',
'L_NAME_JS'		=> 'Name of JS tracker',
'L_NAME_PHP'	=> 'Name of PHP tracker',
'L_JS'			=> 'JavaScript tracking',
'L_IMG'			=> 'Image tracking',
'L_SAVE'		=> 'Save',

);
?>