<?php
/**
 *
 * Plugin	plxMakiPiwik
 * @author	Tetsumaki
 *
 **/
class plxMakiPiwik extends plxPlugin {

	public function __construct($default_lang) {

		# appel du constructeur de la classe plxPlugin (obligatoire)
		parent::__construct($default_lang);

		# limite l'accè a l'écran d'administration du plugin
		$this->setConfigProfil(PROFIL_ADMIN);

		# Déclarations des hooks
		if(!isset($_GET['preview']))
			$this->addHook('ThemeEndBody', 'ThemeEndBody');
	}

	public function ThemeEndBody() {

		if($this->getParam('piwikJs')) {
			$string  = "\n<script type=\"text/javascript\">\n";
			$string .= "	var _paq = _paq || [];\n";
			$string .= "	_paq.push([\"trackPageView\"]);\n";
			$string .= "	_paq.push([\"enableLinkTracking\"]);\n";
			$string .= "	(function() {\n";
			$string .= "		var u=\"".$this->getParam('piwikProtocol')."://".$this->getParam('piwikUrl')."/\";\n";
			$string .= "		_paq.push([\"setTrackerUrl\", u+\"".$this->getParam('piwikNamePhp')."\"]);\n";
			$string .= "		_paq.push([\"setSiteId\", \"".$this->getParam('piwikId')."\"]);\n";
			$string .= "		var d=document, g=d.createElement(\"script\"), s=d.getElementsByTagName(\"script\")[0]; g.type=\"text/javascript\";\n";
			$string .= "		g.defer=true; g.async=true; g.src=u+\"".$this->getParam('piwikNameJs')."\"; s.parentNode.insertBefore(g,s);\n";
			$string .= "	})();\n";
			$string .= "</script>\n";
			echo $string;
		}

		if($this->getParam('piwikImg')) {
			$string = "\n<noscript><img src=\"".$this->getParam('piwikProtocol')."://".$this->getParam('piwikUrl')."/".$this->getParam('piwikNamePhp')."?idsite=".$this->getParam('piwikId')."&amp;rec=1\" style=\"border:0\" alt=\"\" /></noscript>\n";
			echo $string;
		}
	}
}
?>