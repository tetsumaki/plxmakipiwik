<?php if(!defined('PLX_ROOT')) exit; ?>

<?php

# Control du token du formulaire
plxToken::validateFormToken($_POST);

if (!empty($_POST)) {
	$plxPlugin->setParam('piwikProtocol', $_POST['piwikProtocol'], 'string');
	$plxPlugin->setParam('piwikUrl', $_POST['piwikUrl'], 'string');
	$plxPlugin->setParam('piwikId', $_POST['piwikId'], 'numeric');
	$plxPlugin->setParam('piwikNameJs', $_POST['piwikNameJs'], 'string');
	$plxPlugin->setParam('piwikNamePhp', $_POST['piwikNamePhp'], 'string');
	$plxPlugin->setParam('piwikJs', $_POST['piwikJs'], 'numeric');
	$plxPlugin->setParam('piwikImg', $_POST['piwikImg'], 'numeric');
	$plxPlugin->saveParams();
	header('Location: parametres_plugin.php?p=plxMakiPiwik');
	exit;
}
$piwikProtocol = $plxPlugin->getParam('piwikProtocol') == '' ? 'http' : $plxPlugin->getParam('piwikProtocol');
$piwikUrl = $plxPlugin->getParam('piwikUrl') == '' ? 'domain.com/stats' : $plxPlugin->getParam('piwikUrl');
$piwikId = $plxPlugin->getParam('piwikId') == '' ? 1 : $plxPlugin->getParam('piwikId');
$piwikNameJs = $plxPlugin->getParam('piwikNameJs') == '' ? 'piwik.js' : $plxPlugin->getParam('piwikNameJs');
$piwikNamePhp = $plxPlugin->getParam('piwikNamePhp') == '' ? 'piwik.php' : $plxPlugin->getParam('piwikNamePhp');
$piwikJs = $plxPlugin->getParam('piwikJs') == '' ? 1 : $plxPlugin->getParam('piwikJs');
$piwikImg = $plxPlugin->getParam('piwikImg') == '' ? 1 : $plxPlugin->getParam('piwikImg');
?>

<h2><?php echo $plxPlugin->getInfo('title') ?></h2>
<form id="form_plxMakiPiwik" action="parametres_plugin.php?p=plxMakiPiwik" method="post">
	<fieldset>
		<p class="field"><label for="id_piwikProtocol"><?php echo $plxPlugin->lang('L_PROTOCOL') ?></label></p>
		<?php plxUtils::printSelect('piwikProtocol',array('https'=>'https','http'=>'http'), $piwikProtocol);?>
		<p class="field"><label for="id_piwikUrl"><?php $plxPlugin->lang('L_URL') ?>&nbsp;:</label></p>
		<?php plxUtils::printInput('piwikUrl', $piwikUrl, 'text', '60-255') ?>
		<p class="field"><label for="id_piwikId"><?php $plxPlugin->lang('L_ID') ?>&nbsp;:</label></p>
		<?php plxUtils::printInput('piwikId', $piwikId, 'text', '60-255') ?>
		<p class="field"><label for="id_piwikNameJs"><?php $plxPlugin->lang('L_NAME_JS') ?>&nbsp;:</label></p>
		<?php plxUtils::printInput('piwikNameJs', $piwikNameJs, 'text', '60-255') ?>
		<p class="field"><label for="id_piwikNamePhp"><?php $plxPlugin->lang('L_NAME_PHP') ?>&nbsp;:</label></p>
		<?php plxUtils::printInput('piwikNamePhp', $piwikNamePhp, 'text', '60-255') ?>
		<p class="field"><label for="id_piwikJs"><?php echo $plxPlugin->lang('L_JS') ?></label></p>
		<?php plxUtils::printSelect('piwikJs',array('1'=>L_YES,'0'=>L_NO), $piwikJs);?>
		<p class="field"><label for="id_piwikImg"><?php echo $plxPlugin->lang('L_IMG') ?></label></p>
		<?php plxUtils::printSelect('piwikImg',array('1'=>L_YES,'0'=>L_NO), $piwikImg);?>
		<p>
		<?php echo plxToken::getTokenPostMethod() ?>
		<input type="submit" name="submit" value="<?php $plxPlugin->lang('L_SAVE') ?>" />
		</p>
	</fieldset>
</form>
